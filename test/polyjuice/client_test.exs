# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest do
  use ExUnit.Case

  test "transaction_id is unique" do
    client = %Polyjuice.Client{
      base_url: "http://localhost:8008"
    }

    # the best that we can do is test that two calls to transaction_id return
    # different values
    assert Polyjuice.Client.API.transaction_id(client) !=
             Polyjuice.Client.API.transaction_id(client)
  end

  test "sync child spec" do
    client = %Polyjuice.Client{
      base_url: "http://localhost:8008"
    }

    %{id: Polyjuice.Client.Sync, restart: :permanent, start: start} =
      Polyjuice.Client.API.sync_child_spec(client, "listener")

    assert start == {Polyjuice.Client.Sync, :start_link, [[client, "listener"]]}
  end

  test "sync" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetSync{},
             {:ok, %{}}
           }
         } do
      {:ok, %{}} = Polyjuice.Client.sync(client)
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetSync{
               filter: %{},
               since: "token",
               full_state: true,
               set_presence: :offline,
               timeout: 120
             },
             {:ok, %{}}
           }
         } do
      {:ok, %{}} =
        Polyjuice.Client.sync(
          client,
          filter: %{},
          since: "token",
          full_state: true,
          set_presence: :offline,
          timeout: 120
        )
    end
  end
end
