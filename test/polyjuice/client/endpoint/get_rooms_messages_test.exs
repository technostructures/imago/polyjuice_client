# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetRoomsMessagesTest do
  use ExUnit.Case

  test "GET rooms/{roomId}/messages" do
    with endpoint = %Polyjuice.Client.Endpoint.GetRoomsMessages{
           room: "!roomid",
           from: "token",
           dir: :forward
         } do
      http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint, "https://example.com")

      assert http_spec == %Polyjuice.Client.Endpoint.HttpSpec{
               auth_required: true,
               body: "",
               headers: [
                 {"Accept", "application/json"}
               ],
               method: :get,
               url:
                 "https://example.com/_matrix/client/r0/rooms/%21roomid/messages?from=token&dir=f"
             }

      assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
               endpoint,
               200,
               [],
               "{}"
             ) == {:ok, %{}}

      assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
               endpoint,
               500,
               [],
               "Aaah!"
             ) == {:error, 500, "Aaah!"}
    end

    with endpoint = %Polyjuice.Client.Endpoint.GetRoomsMessages{
           room: "!roomid",
           from: "token",
           to: "endtoken",
           dir: :backward,
           limit: 20,
           filter: %{}
         } do
      http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint, "https://example.com")

      assert http_spec == %Polyjuice.Client.Endpoint.HttpSpec{
               auth_required: true,
               body: "",
               headers: [
                 {"Accept", "application/json"}
               ],
               method: :get,
               url:
                 "https://example.com/_matrix/client/r0/rooms/%21roomid/messages?from=token&dir=b&to=endtoken&limit=20&filter=%7B%7D"
             }
    end
  end
end
