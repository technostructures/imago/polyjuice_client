# Copyright 2019-2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule DummyClient do
  require ExUnit.Assertions
  import ExUnit.Assertions
  defstruct [:response]

  defimpl Polyjuice.Client.API do
    def call(%{response: {request, result}}, endpoint) do
      if request != nil do
        assert endpoint == request
      end

      result
    end

    def transaction_id(_), do: "txn_id"

    def sync_child_spec(_client_api, _listener, _opts \\ []) do
      %{}
    end
  end

  defmodule MultiReq do
    require ExUnit.Assertions
    import ExUnit.Assertions
    defstruct [:pid]

    def create(responses) when is_list(responses) do
      {:ok, pid} = Agent.start(fn -> responses end)

      %MultiReq{
        pid: pid
      }
    end

    def destroy(%{pid: pid}) do
      # make sure we weren't expecting any more requests
      remaining = Agent.get(pid, & &1)
      assert remaining == []

      Process.exit(pid, :kill)
    end

    defimpl Polyjuice.Client.API do
      def call(%{pid: pid}, endpoint) do
        {request, result} =
          Agent.get_and_update(
            pid,
            fn
              [head | rest] -> {head, rest}
              [] -> {nil, nil}
            end
          )

        assert request != nil
        assert endpoint == request

        result
      end

      def transaction_id(_), do: "txn_id"

      def sync_child_spec(_client_api, _listener, _opts \\ []) do
        %{}
      end
    end
  end
end
