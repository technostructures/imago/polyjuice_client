defmodule PolyjuiceClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :polyjuice_client,
      description: "Client library for the Matrix.org communications protocol",
      version: "0.2.2",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),

      # Docs
      name: "Polyjuice Client",
      source_url: "https://gitlab.com/uhoreg/polyjuice_client",
      # homepage_url: "https://www.uhoreg.ca/programming/matrix/polyjuice",
      docs: [
        # The main page in the docs
        main: "readme",
        # logo: "path/to/logo.png",
        extras: [
          "README.md",
          "tutorial_echo.md",
          "tutorial_welcome.md"
        ]
      ],
      package: [
        maintainers: ["Hubert Chathi"],
        licenses: ["Apache-2.0"],
        links: %{
          "Source" => "https://gitlab.com/uhoreg/polyjuice_client"
        }
      ],

      # Tests
      test_coverage: [
        summary: [threshold: false]
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:hackney, "~> 1.12"},
      {:poison, "~> 4.0"},
      {:polyjuice_util, "~> 0.1.0"}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
