# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Filter do
  @moduledoc ~S"""
  Build filters.

  https://matrix.org/docs/spec/client_server/r0.5.0#filtering

  The functions in this module can be chained to create more complex filters.

  Examples:

      iex> Polyjuice.Client.Filter.include_state_types(["m.room.member"])
      ...> |> Polyjuice.Client.Filter.limit_timeline_events(10)
      ...> |> Polyjuice.Client.Filter.lazy_loading()
      %{
        "room" => %{
          "state" => %{
            "types" => ["m.room.member"],
            "lazy_load_members" => true
          },
          "timeline" => %{
            "lazy_load_members" => true,
            "limit" => 10
          }
        }
      }

  """

  @doc """
  Update the value in a filter.

  The `key_path` is a list of keys to traverse to find the element to update.
  The `initial` and `func` arguments are like the corresponding arguments to
  `Map.update`.

  Examples:

      iex> Polyjuice.Client.Filter.update(
      ...>   %{
      ...>     "presence" => %{
      ...>       "not_senders" => ["@alice:example.com"]
      ...>     }
      ...>   },
      ...>   ["presence", "not_senders"],
      ...>   ["@bob:example.com"],
      ...>   &Enum.concat(&1, ["@bob:example.com"])
      ...>  )
      %{
        "presence" => %{
          "not_senders" => ["@alice:example.com", "@bob:example.com"]
        }
      }

      iex> Polyjuice.Client.Filter.update(
      ...>   %{
      ...>     "presence" => %{
      ...>       "not_senders" => ["@alice:example.com"]
      ...>     }
      ...>   },
      ...>   ["presence", "senders"],
      ...>   ["@bob:example.com"],
      ...>   &Enum.concat(&1, ["@bob:example.com"])
      ...>  )
      %{
        "presence" => %{
          "not_senders" => ["@alice:example.com"],
          "senders" => ["@bob:example.com"]
        }
      }

  """
  @spec update(map, [String.t()], Any, (Any -> Any)) :: map
  def update(filter, key_path, initial, func)

  def update(filter, [key], initial, func) when is_map(filter) do
    Map.update(filter, key, initial, func)
  end

  def update(filter, [key | rest], initial, func) when is_map(filter) do
    Map.put(filter, key, update(Map.get(filter, key, %{}), rest, initial, func))
  end

  @doc """
  Set the value in a filter.

  The `key_path` is a list of keys to traverse to find the element to update.
  The `initial` and `func` arguments are like the corresponding arguments to
  `Map.update`.

      iex> Polyjuice.Client.Filter.put(
      ...>   %{
      ...>     "presence" => %{
      ...>       "not_senders" => ["@alice:example.com"]
      ...>     }
      ...>   },
      ...>   ["presence", "not_senders"],
      ...>   ["@bob:example.com"]
      ...>  )
      %{
        "presence" => %{
          "not_senders" => ["@bob:example.com"]
        }
      }

  """
  @spec put(map, [String.t()], Any) :: map
  def put(filter, key_path, val)

  def put(filter, [key], val) when is_map(filter) do
    Map.put(filter, key, val)
  end

  def put(filter, [key | rest], val) when is_map(filter) do
    Map.put(filter, key, put(Map.get(filter, key, %{}), rest, val))
  end

  @doc """
  Allow certain types of presence events to be included.
  """
  @spec include_presence_types(filter :: map, types :: list) :: map
  def include_presence_types(filter \\ %{}, types)

  def include_presence_types(filter, types) when filter == %{} and is_list(types) do
    %{
      "presence" => %{
        "types" => types
      }
    }
  end

  def include_presence_types(filter, types) when is_map(filter) and is_list(types) do
    update(
      filter,
      ["presence", "types"],
      types,
      &Enum.concat(&1, types)
    )
  end

  @doc """
  Don't allow certain types of presence events.
  """
  @spec exclude_presence_types(filter :: map, types :: list) :: map
  def exclude_presence_types(filter \\ %{}, types)

  def exclude_presence_types(filter, types) when filter == %{} and is_list(types) do
    %{
      "presence" => %{
        "not_types" => types
      }
    }
  end

  def exclude_presence_types(filter, types) when is_map(filter) and is_list(types) do
    update(
      filter,
      ["presence", "not_types"],
      types,
      &Enum.concat(&1, types)
    )
  end

  @doc """
  Allow certain types of ephemeral room events to be included.
  """
  @spec include_ephemeral_types(filter :: map, types :: list) :: map
  def include_ephemeral_types(filter \\ %{}, types)

  def include_ephemeral_types(filter, types) when filter == %{} and is_list(types) do
    %{
      "room" => %{
        "ephemeral" => %{
          "types" => types
        }
      }
    }
  end

  def include_ephemeral_types(filter, types) when is_map(filter) and is_list(types) do
    update(
      filter,
      ["room", "ephemeral", "types"],
      types,
      &Enum.concat(&1, types)
    )
  end

  @doc """
  Don't allow certain types of ephemeral room events.
  """
  @spec exclude_ephemeral_types(filter :: map, types :: list) :: map
  def exclude_ephemeral_types(filter \\ %{}, types)

  def exclude_ephemeral_types(filter, types) when filter == %{} and is_list(types) do
    %{
      "room" => %{
        "ephemeral" => %{
          "not_types" => types
        }
      }
    }
  end

  def exclude_ephemeral_types(filter, types) when is_map(filter) and is_list(types) do
    update(
      filter,
      ["room", "ephemeral", "not_types"],
      types,
      &Enum.concat(&1, types)
    )
  end

  @doc """
  Allow certain types of state events to be included.
  """
  @spec include_state_types(filter :: map, types :: list) :: map
  def include_state_types(filter \\ %{}, types)

  def include_state_types(filter, types) when filter == %{} and is_list(types) do
    %{
      "room" => %{
        "state" => %{
          "types" => types
        }
      }
    }
  end

  def include_state_types(filter, types) when is_map(filter) and is_list(types) do
    update(
      filter,
      ["room", "state", "types"],
      types,
      &Enum.concat(&1, types)
    )
  end

  @doc """
  Don't allow certain types of state events.
  """
  @spec exclude_state_types(filter :: map, types :: list) :: map
  def exclude_state_types(filter \\ %{}, types)

  def exclude_state_types(filter, types) when filter == %{} and is_list(types) do
    %{
      "room" => %{
        "state" => %{
          "not_types" => types
        }
      }
    }
  end

  def exclude_state_types(filter, types) when is_map(filter) and is_list(types) do
    update(
      filter,
      ["room", "state", "not_types"],
      types,
      &Enum.concat(&1, types)
    )
  end

  @doc """
  Allow certain types of timeline events to be included.
  """
  @spec include_timeline_types(filter :: map, types :: list) :: map
  def include_timeline_types(filter \\ %{}, types)

  def include_timeline_types(filter, types) when filter == %{} and is_list(types) do
    %{
      "room" => %{
        "timeline" => %{
          "types" => types
        }
      }
    }
  end

  def include_timeline_types(filter, types) when is_map(filter) and is_list(types) do
    update(
      filter,
      ["room", "timeline", "types"],
      types,
      &Enum.concat(&1, types)
    )
  end

  @doc """
  Don't allow certain types of timeline events.
  """
  @spec exclude_timeline_types(filter :: map, types :: list) :: map
  def exclude_timeline_types(filter \\ %{}, types)

  def exclude_timeline_types(filter, types) when filter == %{} and is_list(types) do
    %{
      "room" => %{
        "timeline" => %{
          "not_types" => types
        }
      }
    }
  end

  def exclude_timeline_types(filter, types) when is_map(filter) and is_list(types) do
    update(
      filter,
      ["room", "timeline", "not_types"],
      types,
      &Enum.concat(&1, types)
    )
  end

  @doc """
  Set the maximum number of timeline events.
  """
  @spec limit_timeline_events(filter :: map, limit :: integer) :: map
  def limit_timeline_events(filter \\ %{}, limit)

  def limit_timeline_events(filter, limit) when filter == %{} and is_integer(limit) do
    %{
      "room" => %{
        "timeline" => %{
          "limit" => limit
        }
      }
    }
  end

  def limit_timeline_events(filter, limit) when is_map(filter) and is_integer(limit) do
    put(filter, ["room", "timeline", "limit"], limit)
  end

  @spec lazy_loading(filter :: map) :: map
  def lazy_loading(filter \\ %{})

  def lazy_loading(filter) when filter == %{} do
    %{
      "room" => %{
        "state" => %{
          "lazy_load_members" => true
        },
        "timeline" => %{
          "lazy_load_members" => true
        }
      }
    }
  end

  def lazy_loading(filter) when is_map(filter) do
    filter
    |> put(["room", "state", "lazy_load_members"], true)
    |> put(["room", "timeline", "lazy_load_members"], true)
  end
end
