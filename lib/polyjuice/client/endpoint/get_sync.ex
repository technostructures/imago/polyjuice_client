# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetSync do
  @moduledoc """
  Synchronize the latest events from the server

  https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-sync
  """

  @type t :: %__MODULE__{
          filter: String.t() | map() | nil,
          since: String.t() | nil,
          full_state: boolean(),
          set_presence: :offline | :online | :unavailable,
          timeout: integer | nil
        }

  defstruct [
    :filter,
    :since,
    full_state: false,
    set_presence: :online,
    timeout: 0
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(
          %Polyjuice.Client.Endpoint.GetSync{
            filter: filter,
            since: since,
            full_state: full_state,
            set_presence: set_presence,
            timeout: timeout
          },
          base_url
        ) do
      query =
        [
          [
            {"timeout", timeout}
          ],
          if(since, do: [{"since", since}], else: []),
          if(full_state, do: [{"full_state", "true"}], else: []),
          case filter do
            %{} -> [{"filter", Poison.encode!(filter)}]
            nil -> []
            _ -> [{"filter", filter}]
          end,
          case set_presence do
            :offline -> [{"set_presence", "offline"}]
            :unavailable -> [{"set_presence", "unavailable"}]
            :online -> []
          end
        ]
        |> Enum.concat()
        |> URI.encode_query()

      url = %{
        URI.merge(
          base_url,
          "#{Polyjuice.Client.prefix_r0()}/sync"
        )
        | query: query
      }

      %Polyjuice.Client.Endpoint.HttpSpec{
        method: :get,
        headers: [
          {"Accept", "application/json"}
        ],
        url: to_string(url)
      }
    end

    def transform_http_result(_req, status_code, _resp_headers, body) do
      case status_code do
        200 ->
          {:ok, Poison.decode!(body)}

        _ ->
          {:error, status_code, body}
      end
    end
  end
end
