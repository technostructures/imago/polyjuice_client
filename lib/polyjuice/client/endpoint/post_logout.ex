# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostLogout do
  @moduledoc """
  Logs out a session

  https://matrix.org/docs/spec/client_server/latest#post-matrix-client-r0-logout
  """

  @type t :: %__MODULE__{}

  defstruct []

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(
          %Polyjuice.Client.Endpoint.PostLogout{},
          base_url
        )
        when is_binary(base_url) do
      %Polyjuice.Client.Endpoint.HttpSpec{
        method: :post,
        headers: [
          {"Accept", "application/json"},
          {"Content-Type", "application/json"}
        ],
        url:
          URI.merge(
            base_url,
            "#{Polyjuice.Client.prefix_r0()}/logout"
          )
          |> to_string(),
        body: "{}"
      }
    end

    def transform_http_result(_req, status_code, _resp_headers, body) do
      case status_code do
        200 ->
          {:ok}

        _ ->
          {:error, status_code, body}
      end
    end
  end
end
